from django import forms  
from django.contrib.auth.models import User  
from django.contrib.auth.forms import UserCreationForm  
from django.core.exceptions import ValidationError  
from django.forms.fields import EmailField  
from django.contrib.auth.models import Permission  
from django.contrib.contenttypes.models import ContentType
  
class RegisterUserCreationForm(UserCreationForm): 
    CHOICES=[('SuperUser','SuperUser'),
         ('User','User')] 
    username = forms.CharField(label='Username', min_length=5, max_length=150)
    email = forms.EmailField(label='Email')  
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)  
    password2 = forms.CharField(label='Confirm password', widget=forms.PasswordInput)
    user_type = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)
  
    def username_clean(self):  
        username = self.cleaned_data['username'].lower()  
        new = User.objects.filter(username = username)  
        if new.count():  
            raise ValidationError("User Already Exist")  
        return username  
  
    def email_clean(self):  
        email = self.cleaned_data['email'].lower()  
        new = User.objects.filter(email=email)  
        if new.count():  
            raise ValidationError(" Email Already Exist")  
        return email  
  
    def clean_password2(self):  
        password1 = self.cleaned_data['password1']  
        password2 = self.cleaned_data['password2']  
  
        if password1 and password2 and password1 != password2:  
            raise ValidationError("Password don't match")  
        return password2  
  
    def save(self, commit = True):  
        if self.cleaned_data['user_type'] == 'User':
            user1 = User.objects.create_user(self.cleaned_data['username'],  
                self.cleaned_data['email'],  
                self.cleaned_data['password1'],is_staff = True)

            # Content_type of User Model 
            #ct = ContentType.objects.get_for_model(User)
            #Create Permission add_user on User Model 
            #permission = Permission.objects.get(content_type=ct, codename='add_user', name = 'Can add user')
            #user1.user_permissions.add(permission)

            # Change User permission 
            #permission = Permission.objects.get(content_type=ct, codename='change_user', name='Can change user')
            #user1.user_permissions.add(permission)

            # user = User.objects.create_user(  
            #     self.cleaned_data['username'],  
            #     self.cleaned_data['email'],  
            #     self.cleaned_data['password1']  
            # ) 
            return user1
        else:
            # Permission QuerySet 
            # permissions = Permission.objects.all()
            ct1= ContentType.objects.get(app_label='auth', model='user')
            #ct = ContentType.objects.get_for_model(User)
            user1 = User.objects.create_user(self.cleaned_data['username'],  
                self.cleaned_data['email'],  
                self.cleaned_data['password1'],is_staff = True)
            # ALL Permissions Granted to SuperUser
            # for permission in permissions:
            #     user1.user_permissions.add(permission)
            #user1.is_superuser = True
            #user1.is_staff = True
            permission1, created1 = Permission.objects.get_or_create(codename='super_user2', name='Super user created',content_type=ct1 )
            #permission2, created2 = Permission.objects.get_or_create( codename='kill_bill', name='kill_bill',content_type=ct1)
            user1.user_permissions.add(permission1)            
            user1 = User.objects.get(username = self.cleaned_data['username'])
            return user1