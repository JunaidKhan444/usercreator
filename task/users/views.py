from django.http import HttpResponse
from django.views.generic.edit import FormView
from django.contrib.auth import login
from django.contrib import messages
from .forms import RegisterUserCreationForm 
from django.contrib.auth.models import Permission , User 
# Create your views here.  
class Register(FormView):
    template_name = 'users/register.html'
    form_class = RegisterUserCreationForm
    
    def form_valid(self, form):
        form.save()
        username1 = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')
        user = User.objects.get(username = username1)

        if user.has_perm('auth.super_user2'):
            #login(self.request, user)
            #messages.success(self.request, "Super User Created")
            return HttpResponse("<h1>Authorised to Access</h1>")
        else:
            return HttpResponse('<h1> Unauthorised to Access </h1>')
        
        